<?php

namespace App\Console\Commands;

use App\Jobs\SendMessage;
use App\lib\Queue\JobContext;
use App\Services\MessagesService;
use Illuminate\Console\Command;

class SendMessages extends Command
{
    /**
     * @var MessagesService
     */
    private $messagesService;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'messages:send-all';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(MessagesService $service)
    {
        $this->messagesService = $service;

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->messagesService->sendAllOnce();
        $messageQueue = $this->messagesService->getUnsentMessages();
        $jobContext = new JobContext();
        $jobContext->releaseDelay = 300;
//        $jobContext->tries = 3;

        foreach ($messageQueue as $message) {
            SendMessage::dispatch($message->id, $jobContext);
        }
    }
}
