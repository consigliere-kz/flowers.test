<?php

namespace App\Jobs;

use App\lib\MessageTransport\exceptions\SendMessageException;
use App\lib\MessageTransport\TransportInterface;
use App\lib\Queue\JobContext;
use App\Models\UserMessage;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class SendMessage implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var int
     */
    public $tries = 1;

    /**
     * @var int
     */
    private $messageId;

    /**
     * @var JobContext
     */
    private $context;

    /**
     * Create a new job instance.
     *
     * @param JobContext $context
     * @param int $messageId
     */
    public function __construct(int $messageId, JobContext $context)
    {
        //можно получать и модель сообщения, но тогда все равно придется делать SELECT * FOR UPDATE
        $this->messageId = $messageId;
        $this->context = $context;
        $this->tries = $context->tries ?: $this->tries;
    }

    /**
     * Execute the job.
     *
     * @param TransportInterface $transport
     * @return void
     * @throws \Exception
     */
    public function handle(TransportInterface $transport)
    {
        DB::beginTransaction();

        /**
         * @var UserMessage $userMessage
         */
        $userMessage = UserMessage::where('id', $this->messageId)
            ->lockForUpdate()
            ->first();

        if (!$userMessage) {
            $this->log('Message not found');
            DB::commit();

            return;
        }

        try {
            if ($userMessage->isReadyForSent()) {
                $userMessage->attempts++;
                $transport->send($userMessage->message, $userMessage->user);
                $userMessage->status = UserMessage::STATUS_SUCCESS;
                $this->log('Message sent');
            } else {
                $this->fail();
            }
        } catch (SendMessageException $e) {
            $userMessage->status = UserMessage::STATUS_FAILED;
            $this->log('Unable to send message due to ' . $e->getMessage());

            $this->rerun();
        } finally {
            $userMessage->save();
            DB::commit();
        }
    }

    /**
     * @param string $message
     */
    protected function log(string $message): void
    {
        $message = 'Message #' . $this->messageId . '. ' . $message . PHP_EOL;

        if (App::isLocal()) {
            echo $message;
        }

        Log::channel($this->context->logChannel)
            ->info($message);
    }

    /**
     * @return void
     */
    private function rerun(): void
    {
        if ($this->attempts() < $this->tries) {
            $this->release($this->context->releaseDelay);
        }
    }
}
