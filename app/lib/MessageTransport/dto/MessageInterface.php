<?php

namespace App\lib\MessageTransport\dto;

/**
 * Interface MessageInterface
 * @package App\lib\MessageTransport\dto
 */
interface MessageInterface
{
    /**
     * @return string
     */
    public function getText(): string;

    //public function getSubject(): string;
    //etc...
}