<?php

namespace App\lib\MessageTransport\exceptions;

/**
 * Class SendMessageException
 * @package App\lib\MessageTransport\exceptions
 */
class SendMessageException extends \Exception
{
}