<?php

namespace App\lib\MessageTransport;
use App\lib\MessageTransport\dto\DeviceInterface;
use App\lib\MessageTransport\dto\MessageInterface;
use App\lib\MessageTransport\exceptions\SendMessageException;

/**
 * Class RandomTransport
 * @package App\lib\MessageTransport
 */
class RandomTransport implements TransportInterface
{
    /**
     * @var int
     */
    public $failPercentage = 60;

    /**
     * @param MessageInterface $message
     * @param DeviceInterface $recipient
     * @throws SendMessageException
     */
    public function send(MessageInterface $message, DeviceInterface $recipient): void
    {
        if (rand(0, 100) < $this->failPercentage) {
            throw new SendMessageException('Shit happens :(');
        }

        //successful send
    }

}