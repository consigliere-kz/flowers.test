<?php

namespace App\Services;
use App\Models\Message;
use App\Models\User;
use App\Models\UserMessage;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;

/**
 * Сервис отвечает за добавление сообщений на отправку пользователям.
 *
 *
 * Class MessagesService
 * @package App\Services
 */
class MessagesService
{
    const DEFAULT_MAX_SEND_ATTEMPTS = 3;

    /**
     * @param Message $message
     * @param User $recipient
     * @return UserMessage
     */
    public function sendTo(Message $message, User $recipient, $maxAttempts = self::DEFAULT_MAX_SEND_ATTEMPTS): UserMessage
    {
        return UserMessage::create([
            'message_id' => $message->id,
            'user_id' => $recipient->id,
            'max_attempts' => $maxAttempts
        ]);
    }

    /**
     * @return void
     */
    public function sendAllOnce(): void
    {
        $allMessages = Message::all(['id']);

        foreach ($allMessages as $message) {
            $this->sendOnceToAll($message);
        }
    }

    /**
     * @param Message $message
     * @param int $maxAttempts
     * @return void
     */
    public function sendOnceToAll(Message $message, $maxAttempts = self::DEFAULT_MAX_SEND_ATTEMPTS): void
    {
        $newRows = [];
        /**
         * @var User[] $recipients
         */
        $recipients = User::whereDoesntHave('sentMessages', function ($query) use ($message) {
            return $query->where('message_id', $message->id);
        })->get(['id']);

        foreach ($recipients as $recipient) {
            $newRows[] = [
                'message_id' => $message->id,
                'user_id' => $recipient->id,
                'max_attempts' => $maxAttempts
            ];
        }

        UserMessage::insert($newRows);
    }

    /**
     * @return array
     */
    public function getUnsentMessages(): Collection
    {
        return UserMessage::where('status', '<>', UserMessage::STATUS_SUCCESS)
            ->where('attempts', '<', DB::raw('max_attempts'))
            ->get();
    }
}