<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UserMessage
 * @package App\Models
 *
 * @property int $id
 * @property int $user_id
 * @property int $message_id
 * @property string $status
 * @property int $attempts
 * @property int $max_attempts
 *
 * @property User $user
 * @property Message $message
 */
class UserMessage extends Model
{
    const STATUS_PENDING = 'pending';
    const STATUS_SUCCESS = 'success';
    const STATUS_FAILED = 'failed';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'user_message';

    /**
     * @var array
     */
    protected $fillable = ['user_id', 'message_id', 'max_attempts'];

    /**
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function message()
    {
        return $this->belongsTo(Message::class);
    }

    /**
     * @return bool
     */
    public function isReadyForSent(): bool
    {
        return $this->status != self::STATUS_SUCCESS
            && $this->attempts < $this->max_attempts;
    }
}
