<?php

namespace App\Models;

use App\lib\MessageTransport\dto\MessageInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Message
 * @package App\Models
 *
 * @property int $id
 * @property string $text
 */
class Message extends Model implements MessageInterface
{
    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'message';

    /**
     * @var array
     */
    protected $fillable = ['text'];

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->getText();
    }
}
