<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UsersMessages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_message', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('message_id');
            $table->foreign('user_id', 'fk-user_message-user')
                ->references('id')
                ->on('user')
                ->onDelete('CASCADE');
            $table->foreign('message_id', 'fk-user_message-message')
                ->references('id')
                ->on('message')
                ->onDelete('CASCADE');
            $table->string('status', 32)
                ->default('pending');
            $table->integer('attempts')
                ->default(0);
            $table->integer('max_attempts')
                ->default(1);
            $table->index('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_message', function (Blueprint $table) {
            $table->dropForeign('fk-user_message-user');
            $table->dropForeign('fk-user_message-message');
        });
        Schema::dropIfExists('user_message');
    }
}
